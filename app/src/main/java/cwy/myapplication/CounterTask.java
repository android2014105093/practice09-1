package cwy.myapplication;

import android.os.AsyncTask;
import android.widget.TextView;

/**
 * Created by SSELAB on 2016-11-01.
 */

public class CounterTask extends AsyncTask<Integer, Integer, Integer> {

    TextView mOutput;
    int mSum = 0;

    public void setOutputView(TextView txt) {
        mOutput = txt;
    }
    @Override
    protected void onPreExecute() {
        mSum = 0;
        super.onPreExecute();
    }
    @Override
    protected Integer doInBackground(Integer... params) {
        for (int i = 1; i <= params[0]; i++) {
            mSum += i;
            publishProgress(mSum);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {;}
        }
        return mSum;
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        mOutput.setText("진행상황: " + values[0] + "\n"
                + "현재까지 합: " + mSum);
        super.onProgressUpdate(values);
    }
    @Override
    protected void onPostExecute(Integer integer) {
        mOutput.setText("결과: " + mSum);
        super.onPostExecute(integer);
    }
    @Override
    protected void onCancelled(Integer integer) {
        mOutput.setText("취소됨");
        super.onCancelled();
    }
}
