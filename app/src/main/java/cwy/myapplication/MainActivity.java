package cwy.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    CounterTask mTask;
    boolean mBreak = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnStart = (Button) findViewById(R.id.button);
        Button btnCancel = (Button) findViewById(R.id.button2);
        btnStart.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void startWork(View v) {
        if (mBreak) {
            mBreak = false;
            mTask = new CounterTask();
            mTask.setOutputView((TextView) findViewById(R.id.textView3));
            mTask.execute(Integer.parseInt(((EditText) findViewById(R.id.editText)).getText().toString()));
        } else {
            mBreak = true;
        }
    }

    public void cancelWork(View v) {
        mTask.cancel(true);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.button:
                startWork(view);
                break;
            case R.id.button2:
                cancelWork(view);
                break;
        }
    }
}
